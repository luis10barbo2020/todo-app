import { type NextPage } from "next";
import { useEffect, useState } from "react";
import { SelectButtonComponent } from "../components/button";
import SelectComponent, { type SelectOption } from "../components/select";
import TodoCardComponent from "../components/todo/TodoCard";
import type { TodoCard, TodoCardFilter, TodoCardSorting } from "../types/todo";

import { api } from "../utils/api";
import { objectChildState } from "../utils/react";

const Home: NextPage = () => {
  const hello = api.example.hello.useQuery({ text: "from tRPC" });
  const [cards, setCards] = useState<TodoCard[]>([]);

  useEffect(() => {
    setCards([
      {
        id: "3",
        content: "Learn gitlab on udemy",
        dateCreated: new Date(100),
        title: "Learn gitlab",
        status: "pending",
      },
      {
        id: "2",
        content: "Learn git on udemy",
        dateCreated: new Date(1000),
        title: "Learn git",
        status: "completed",
      },
      {
        id: "1",
        content: "Create website",
        dateCreated: new Date(40000),
        title: "Create website",
        status: "cancelled",
      },
    ]);
  }, [setCards]);

  const [sorting, setSorting] = useState<SelectOption>({
    node: "Date",
    value: "undefined",
  });
  const [filter, setFilter] = useState<TodoCardFilter>({
    completed: true,
    pending: true,
    cancelled: true,
  });

  const cardComponents = generateCardComponents(
    cards,
    sorting.value as TodoCardSorting,
    filter
  );

  return (
    <main className="flex h-screen w-full flex-col items-center">
      <div className="flex flex-col gap-2 p-4">
        <div className="flex flex-wrap justify-center gap-2">
          <SelectButtonComponent
            reactState={objectChildState([filter, setFilter], "completed")}
            colorTint="rgb(248, 255, 231)"
            label="Completed"
          />
          <SelectButtonComponent
            reactState={objectChildState([filter, setFilter], "pending")}
            colorTint="rgb(242, 242, 242)"
            label="Pending"
          />
          <SelectButtonComponent
            reactState={objectChildState([filter, setFilter], "cancelled")}
            colorTint="rgb(255, 231, 231)"
            label="Cancelled"
          />
        </div>
        <div className="flex items-center justify-center gap-2">
          Sort by:
          <SelectComponent
            options={[
              { node: <>Date</>, value: "date" },
              { node: <>Status</>, value: "status" },
            ]}
            reactState={[sorting, setSorting]}
          ></SelectComponent>
        </div>
      </div>

      <div className="todo-holder flex h-full w-full max-w-[840px] flex-col gap-4 overflow-auto p-4">
        {cardComponents.length > 0 ? (
          cardComponents
        ) : (
          <p className=" text-center text-2xl">
            No matching cards found, try tweaking your filter options...
          </p>
        )}
      </div>
    </main>
  );
};

function generateCardComponents(
  todoCardList: TodoCard[],
  sorting: TodoCardSorting,
  filter: TodoCardFilter
) {
  const sortPriority = {
    pending: 1,
    completed: 2,
    cancelled: 3,
  };

  const sortedCards = todoCardList.sort((a, b) => {
    switch (sorting) {
      case "status":
        if (a.status === b.status) return 0;
        return sortPriority[a.status] - sortPriority[b.status];
      case "date":
        return b.dateCreated.getSeconds() - a.dateCreated.getSeconds();
    }
  });

  const result = sortedCards.flatMap((card, index) => {
    if (filter[card.status])
      return <TodoCardComponent content={card} key={index}></TodoCardComponent>;
    return [];
  });
  return result;
}

export default Home;
