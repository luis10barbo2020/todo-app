import { z } from "zod";

export const zodTodoCard = z.object({
  id: z.string(),
  title: z.string(),
  content: z.string(),
  dateCreated: z.date(),
  status: z.union([
    z.literal("completed"),
    z.literal("cancelled"),
    z.literal("pending"),
  ]),
});

export const zodTodoCardFilter = z.object({
  completed: z.boolean(),
  cancelled: z.boolean(),
  pending: z.boolean(),
});

export const zodTodoCardSorting = z.union([
  z.literal("date"),
  z.literal("status"),
]);

export type TodoCard = z.infer<typeof zodTodoCard>;
export type TodoCardFilter = z.infer<typeof zodTodoCardFilter>;
export type TodoCardSorting = z.infer<typeof zodTodoCardSorting>;
