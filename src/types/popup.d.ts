import { ReactNode } from "react";
import { z } from "zod";

export const zodPopupPage = z.object({
  content: z.custom<ReactNode>(),
});
export const zodPopupType = z.object({
  id: z.string().uuid().default(),
  pages: z.array(zodPopupPage),
  title: z.string(),
  currentPage: z.number(),
  lastPage: z.number().optional(),
  nextPage: z.number().optional(),
});

export type PopupContextType = {};
export type PopupType = z.infer<typeof zodPopupType>;
export type PopupPage = z.infer<typeof zodPopupPage>;
