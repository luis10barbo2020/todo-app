import React from "react";
import type { Dispatch, SetStateAction } from "react";
import { reverseState } from "../utils/react";

export const ButtonComponent: React.FC<{
  children?: React.ReactNode;
  label?: string;
  onClick: React.MouseEventHandler<HTMLButtonElement>;
}> = ({ children, label, onClick }) => {
  return (
    <button
      onClick={onClick}
      className="rounded-md border p-2 duration-75 hover:border-black/40"
    >
      {label || children}
    </button>
  );
};

export const SelectButtonComponent: React.FC<{
  children?: React.ReactNode;
  label?: string;
  reactState: [boolean, Dispatch<SetStateAction<boolean>>];
  colorTint?: string;
}> = ({
  children,
  label,
  reactState: [isSelected, setSelected],
  colorTint,
}) => {
  return (
    <button
      onClick={() => {
        reverseState(setSelected);
      }}
      style={{ backgroundColor: colorTint ? colorTint : "" }}
      className={`rounded-md border p-2 duration-75 hover:border-black/40 ${
        isSelected
          ? `opacity-100 ${colorTint ? `bg-[${colorTint}] ` : ""}`
          : "opacity-30"
      }`}
    >
      {label || children}
    </button>
  );
};
