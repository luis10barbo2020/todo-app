import { useEffect, useState } from "react";
import type { TodoCard } from "../../types/todo";

const TodoCardComponent: React.FC<{ content: TodoCard }> = ({ content }) => {
  const [isHover, setHover] = useState(false);
  const [textColor, setTextColor] = useState<string>("text-gray-500");

  useEffect(() => {
    setTextColor(getNewTextColor(content.status));
  }, [content.status]);

  return (
    <div
      className="flex h-[200px] w-full rounded-md  shadow-[0_0_6px_1px] shadow-black/20"
      onMouseEnter={() => {
        setHover(true);
      }}
      onMouseLeave={() => {
        setHover(false);
      }}
    >
      <div className="flex flex-col  overflow-y-auto p-4">
        <p className="card-title text-2xl">{content.title}</p>
        <p>{content.content}</p>
        <p className={`${textColor} text-gray`}>{content.status}</p>

        <p className="mt-auto">{content.dateCreated.toString()}</p>
      </div>
      <div
        className={`card-right ml-auto h-full min-w-[48px]  max-w-full overflow-x-hidden ${
          isHover ? "min-w-[48px] max-w-full" : "min-w-0  max-w-0 "
        }  duration-150`}
      >
        <div className="my-2 flex h-10 w-10 cursor-pointer items-center justify-center rounded-md border duration-75 hover:border-black/40">
          ✓
        </div>
        <div className="my-2 flex h-10 w-10 cursor-pointer items-center justify-center rounded-md border duration-75 hover:border-black/40">
          ✕
        </div>
        <div className="my-2 flex h-10 w-10 cursor-pointer items-center justify-center rounded-md border duration-75 hover:border-black/40">
          ✎
        </div>
        <div className="my-2 flex h-10 w-10 cursor-pointer items-center justify-center rounded-md border duration-75 hover:border-black/40">
          🕑
        </div>
      </div>
    </div>
  );
};

function getNewTextColor(status: TodoCard["status"]) {
  switch (status) {
    case "completed":
      return "text-green-500";

    case "pending":
      return "text-gray-500";

    case "cancelled":
      return "text-red-500";
  }
}

export default TodoCardComponent;
