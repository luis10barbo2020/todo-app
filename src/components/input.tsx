import type { Dispatch, SetStateAction } from "react";

const TextInputComponent: React.FC<{
  reactState: [string, Dispatch<SetStateAction<string>>];
  placeholder: string;
}> = ({ reactState: [text, setText], placeholder }) => {
  return (
    <input
      type="text"
      className="w-full rounded-md border p-2 outline-none duration-75 hover:border-black/40 focus:border-blue-300"
      placeholder={placeholder || ""}
      value={text}
      onChange={(e) => {
        setText(e.target.value);
      }}
    />
  );
};

export default TextInputComponent;
