import { LegacyRef, useEffect, useRef, useState } from "react";
import { elementFocusHandler, reverseState } from "../utils/react";

import type { Dispatch, SetStateAction } from "react";

export type SelectOption = { node: React.ReactNode; value: string };
const SelectComponent: React.FC<{
  options: SelectOption[];
  reactState: [SelectOption, Dispatch<SetStateAction<SelectOption>>];
}> = ({ options, reactState: [selectedOption, setSelectedOption] }) => {
  const [isFocused, setFocus] = useState(false);

  useEffect(() => {
    const firstOption = options[0];
    if (firstOption) setSelectedOption(firstOption);
  }, []);

  useEffect(() => {
    if (menuRef.current)
      return elementFocusHandler(menuRef.current as HTMLElement, setFocus);
  }, []);

  const menuRef = useRef<HTMLElement>(null);

  return (
    <div
      className={`relative w-32 cursor-pointer select-none rounded-md border p-2 duration-75 ${
        isFocused ? "border-black/40" : "hover:border-black/40"
      }`}
      // onClick={() => {
      //   reverseState(setFocus);
      // }}
      ref={menuRef as LegacyRef<HTMLDivElement>}
    >
      <p className="title flex">
        <span className="w-full text-center">{selectedOption?.node}</span>
        <span className="ml-auto">▾</span>
      </p>
      <div
        style={{ height: isFocused ? `${40 * options.length + 1}px` : "0" }}
        className={`absolute left-0 top-[41px] flex w-full cursor-default flex-col overflow-hidden  rounded-b-md border-x border-b bg-white duration-150 ${
          isFocused
            ? " overflow-y-auto border-black/10"
            : "overflow-y-hidden border-transparent"
        }`}
      >
        {options.map((option, index) => {
          return (
            <button
              key={index}
              onClick={() => {
                setSelectedOption(option);
              }}
              className="max-h-[40px] p-2 hover:bg-gray-100"
            >
              {option.node}
            </button>
          );
        })}
      </div>
    </div>
  );
};
export default SelectComponent;
