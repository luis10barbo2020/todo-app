import { createContext, useContext, useEffect, useState } from "react";
import {
  PopupContextType,
  PopupPage,
  PopupType,
  zodPopupType,
} from "../types/popup";
import { v4 } from "uuid";

type CreatePopupInformation = Omit<
  PopupType,
  "currentPage" | "id" | "lastPage" | "nextPage"
>;

export const PopupContext = createContext<{
  createPopup: (popupPages: CreatePopupInformation) => void;
  deletePopup: (id: PopupType["id"]) => void;
}>({ createPopup: () => {}, deletePopup: () => {} });

export const PopupProvider: React.FC<{
  children: React.ReactNode;
}> = ({ children }) => {
  const [popups, setPopups] = useState<PopupType[]>([]);

  function createPopup(information: CreatePopupInformation) {
    const newPopup: PopupType = {
      id: v4(),
      pages: information.pages,
      currentPage: 0,
      title: information.title,
    };
    setPopups((oldState) => {
      return [...oldState, newPopup];
    });
  }

  function deletePopup(id: PopupType["id"]) {
    setPopups((oldState) => {
      return oldState.flatMap((popup) => {
        if (id === popup.id) return [];
        return popup;
      });
    });
  }

  useEffect(() => {
    createPopup({
      pages: [{ content: <>here</> }, { content: <>here</> }],
      title: "Create todo",
    });
  }, []);

  return (
    <PopupContext.Provider value={{ createPopup, deletePopup }}>
      {popups.map((popup, index) => {
        return (
          <div
            className="absolute z-10 flex h-screen w-screen items-center justify-center bg-black/10"
            key={index}
          >
            <div className="m-8 h-full max-h-[400px] w-full max-w-[600px] rounded-lg bg-white shadow-[0_0_6px_1px_rgba(0,0,0,0.2)]">
              {(() => {
                const currentPage = popup.pages.at(popup.currentPage);
                if (currentPage)
                  return (
                    <PopupPageComponent
                      popupPage={currentPage}
                      popupInformation={popup}
                    ></PopupPageComponent>
                  );

                const defaultPage = popup.pages.at(0);
                if (defaultPage)
                  return (
                    <PopupPageComponent
                      popupPage={defaultPage}
                      popupInformation={popup}
                    ></PopupPageComponent>
                  );

                return <>No pages found</>;
              })()}
            </div>
          </div>
        );
      })}

      <div>{children}</div>
    </PopupContext.Provider>
  );
};

const PopupPageComponent: React.FC<{
  popupPage: PopupPage;
  popupInformation: PopupType;
}> = ({ popupPage, popupInformation }) => {
  const { deletePopup } = useContext(PopupContext);

  return (
    <div>
      <div className="titlebar flex border-b border-black/20 p-4 text-xl">
        {popupInformation.title}
        <div className="ml-auto flex gap-2">
          {popupInformation.lastPage && (
            <button className="rounded-sm border border-black/30 px-2 duration-75 hover:border-black/50">
              ◂
            </button>
          )}
          {popupInformation.nextPage && (
            <button className="rounded-sm border border-black/30 px-2 duration-75 hover:border-black/50">
              ▸
            </button>
          )}
          <button
            className="rounded-sm border border-black/30 px-2 duration-75 hover:border-black/50"
            onClick={() => {
              deletePopup(popupInformation.id);
            }}
          >
            ✕
          </button>
        </div>
      </div>
      {popupPage.content}
    </div>
  );
};
